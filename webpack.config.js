var path = require("path")
var webpack = require("webpack")

const CompressionPlugin = require("compression-webpack-plugin")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const WebpackMd5Hash = require("webpack-md5-hash")

module.exports = function () {
    let env = { prod: /production/i.test(process.env.NODE_ENV) }

    let config = {
        entry: "./src/main.js",
        output: {
            path: path.resolve(__dirname, "./dist"),
            pathinfo: true,
            filename: "[name].js"
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    loader: "vue-loader",
                    exclude: [
                        path.resolve(__dirname, "./src/index.html")
                    ],
                    options: {
                        loaders: {
                            css: ExtractTextPlugin.extract({
                                loader: "css-loader",
                                fallbackLoader: "vue-style-loader"
                            })
                        }
                    }
                },
                {
                    test: /\.js$/,
                    loader: "babel-loader",
                    exclude: /node_modules/
                },
                {
                    test: /\.(png|jpg|gif|svg)$/,
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]?[hash]"
                    }
                }
            ]
        },
        resolve: {
            extensions: [".js"],
            alias: {
                "vue$": "vue/dist/vue"
            }
        },
        devtool: "eval-source-map",
        plugins: [
            new CopyWebpackPlugin([{
                from: "./assets",
                to: "assets"
            }]),
            new ExtractTextPlugin("style.css"),
            new HtmlWebpackPlugin({
                template: "src/index.html"
            })
        ]
    }

    if (env.prod) {
        config.output.filename = "[name].[hash].bundle.js"
        config.output.pathinfo = false
        config.devtool = "source-map"
        config.plugins = [
            ...config.plugins || [],
            new webpack.DefinePlugin({ "process.env": { NODE_ENV: '"production"' } }),
            new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } }),
            new webpack.LoaderOptionsPlugin({ minimize: true }),
            new CompressionPlugin({ regExp: /\.css$|\.html$|\.js$|\.map$/, threshold: 1024 }),
            new WebpackMd5Hash()
        ]
    }

    return config
}
