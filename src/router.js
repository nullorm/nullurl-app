import Vue from "vue"
import VueRouter from "vue-router"

import Dashboard from "./pages/dashboard.html"
import SignIn from "./pages/sign-in.html"

Vue.use(VueRouter)

const routes = [
    {
        path: "/dashboard",
        component: Dashboard,
        beforeEnter: (to, from, next) => {
            let token = localStorage.getItem("token")
            if (!token) { return next("/sign-in") }
            return next(true)
        }
    },
    { path: "/sign-in", component: SignIn },
    { path: "/sign-up", component: SignIn },
    { path: "*", redirect: "/dashboard" }
]

export const router = new VueRouter({
    mode: "history",
    routes
})
