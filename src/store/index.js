import * as auth from "./modules/auth"
import * as uri from "./modules/uri"
import Vue from "vue"
import Vuex from "vuex"
import createLogger from "vuex/dist/logger"

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== "production"

export const store = new Vuex.Store({
    modules: { auth, uri },
    strict: debug,
    plugins: debug ? [createLogger()] : []
})
