import * as types from "../mutation-types"
import * as url from "../../api/url"

function computeShortUrl(id) {
    let matches = id.match(/.{3}/g)
    let characterPool = [
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "_", "-"
    ]
    return matches
        .map(m => {
            let value = parseInt(m, 16) / 64
            let msb = Math.floor(value)
            let lsb = (value - msb) * 64
            return characterPool[msb] + characterPool[lsb]
        })
        .join("")
}

// initial state
export const state = {
    adding: false,
    finding: false,
    list: [],
    removing: false,
    updating: false
}

export const getters = {
    urlList: state => state.list
}

// side effects
export const actions = {
    addURI({ commit }, uri) {
        commit(types.ADD_URI, { uri })
        url.add(uri)
            .then(uri => commit(types.ADD_URI_SUCCESS, { uri }))
            .catch(err => commit(types.ADD_URI_FAIL, { err }))
    },

    findURI({commit}) {
        commit(types.FIND_URI)
        url.find()
            .then(uriList => commit(types.FIND_URI_SUCCESS, { uriList }))
            .catch(err => commit(types.FIND_URI_FAIL, { err }))
    },

    removeURI({commit}, uri) {
        commit(types.REMOVE_URI)
        url.remove(uri)
            .then(uri => commit(types.REMOVE_URI_SUCCESS, { uri }))
            .catch(err => commit(types.REMOVE_URI_FAIL, { err }))
    }
}

// mutations (reducers)
export const mutations = {
    [types.ADD_URI](state) {
        state.adding = true
    },

    [types.ADD_URI_FAIL](state) {
        state.adding = false
    },

    [types.ADD_URI_SUCCESS](state, { uri }) {
        state.adding = false
        uri.short = computeShortUrl(uri._id)
        state.list.push(uri)
    },

    [types.FIND_URI](state) {
        state.finding = true
    },

    [types.FIND_URI_FAIL](state) {
        state.finding = false
    },

    [types.FIND_URI_SUCCESS](state, { uriList }) {
        state.finding = false
        state.list = [...state.list, ...uriList.map(uri => Object.assign(uri, { short: computeShortUrl(uri._id) }))]
    },

    [types.REMOVE_URI](state) {
        state.removing = true
    },

    [types.REMOVE_URI_FAIL](state) {
        state.removing = false
    },

    [types.REMOVE_URI_SUCCESS](state, { uri }) {
        state.removing = false
        let index = state.list.findIndex(u => u._id === uri._id)
        state.list.splice(index, 1)
    }
}
