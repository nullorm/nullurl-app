import * as auth from "../../api/auth"
import * as types from "../mutation-types"

import { router } from "../../router"

// initial state
export const state = {
    authenticating: false,
    token: null,
    tokenData: null
}

// side effects
export const actions = {
    signIn({ commit }, user) {
        commit(types.SIGN_IN, { user })
        auth.signIn(user)
            .then(token => commit(types.AUTH_SUCCESS, { token }))
            .then(() => router.push("dashboard"))
            .catch(err => commit(types.AUTH_FAIL, { err }))
    },
    signUp({ commit }, user) {
        commit(types.SIGN_UP, { user })
        auth.signUp(user)
            .then(token => commit(types.AUTH_SUCCESS, { token }))
            .then(() => router.push("dashboard"))
            .catch(err => commit(types.AUTH_FAIL, { err }))
    }
}

// mutations (reducers)
export const mutations = {
    [types.SIGN_IN](state) {
        state.authenticating = true
    },

    [types.SIGN_UP](state) {
        state.authenticating = true
    },

    [types.AUTH_FAIL](state) {
        state.authenticating = false
    },

    [types.AUTH_SUCCESS](state, { token }) {
        state.authenticating = false
        state.token = token
        state.tokenData = JSON.parse(atob(token.split(".")[1]))
    }
}
