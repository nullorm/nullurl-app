export const SIGN_IN = "[Auth] sign in attempt"
export const SIGN_UP = "[Auth] sign up attempt"
export const AUTH_SUCCESS = "[Auth] sign in/up attempt successful"
export const AUTH_FAIL = "[Auth] sign in/up attempt failed"

export const ADD_URI = "[Uri] add uri attempt"
export const ADD_URI_SUCCESS = "[Uri] add uri attempt successful"
export const ADD_URI_FAIL = "[Uri] add uri attempt failed"

export const FIND_URI = "[Uri] find uri attempt"
export const FIND_URI_SUCCESS = "[Uri] find uri attempt successful"
export const FIND_URI_FAIL = "[Uri] find uri attempt failed"

export const REMOVE_URI = "[Uri] remove uri attempt"
export const REMOVE_URI_SUCCESS = "[Uri] remove uri attempt successful"
export const REMOVE_URI_FAIL = "[Uri] remove uri attempt failed"
