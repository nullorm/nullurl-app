import "regenerator-runtime/runtime"
import Vue from "vue"

import { router } from "./router"
import { store } from "./store"

new Vue({ store, router }).$mount("#app")
