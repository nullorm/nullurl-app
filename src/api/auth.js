import { sendRequest } from "./relay"

export async function signIn(user) {
    // nus
    let uri = "api/users/sign-in"
    let body = JSON.stringify(user)
    let payload = await sendRequest(uri, body)
    localStorage.setItem("token", payload.token)
    return payload.token
}

export async function signUp(user) {
    // nus
    let uri = "api/users/sign-up"
    let body = JSON.stringify(user)
    let payload = await sendRequest(uri, body)
    localStorage.setItem("token", payload.token)
    return payload.token
}
