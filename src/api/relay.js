import { router } from "../router"

let cache = "default"
let method = "POST"

export async function sendRequest(uri, body) {
    let headers = new Headers()
    headers.append("Accept", "application/json")
    headers.append("Content-Type", "application/json")
    let token = localStorage.getItem("token")
    if (token) {
        headers.append("Authorization", `Bearer ${token}`)
    }
    let config = { cache, headers, method }
    if (body) { config.body = body }
    let response = await fetch(uri, config)
    if (response.status === 401) {
        localStorage.clear()
        router.push("sign-in")
        throw new Error("Unauthorized")
    }
    let payload = await response.json()
    if (payload.error) { throw new Error(payload.error) }
    return payload.data
}
