import { sendRequest } from "./relay"

export async function add(url) {
    // nullurl
    let uri = "api/uri/add"
    let body = JSON.stringify(url)
    let payload = await sendRequest(uri, body)
    return payload
}

export async function find() {
    // nullurl
    let uri = "api/uri/fetchAll"
    let payload = await sendRequest(uri)
    return payload
}

export async function remove(url) {
    // nullurl
    let uri = "api/uri/delete"
    let body = JSON.stringify(url)
    let payload = await sendRequest(uri, body)
    return payload
}

export async function update(url) {
    // nullurl
    let uri = "api/uri/update"
    let body = JSON.stringify(url)
    let payload = await sendRequest(uri, body)
    return payload
}
